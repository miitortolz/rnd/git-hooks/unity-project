#region File Header
// Filename: Builder.cs
// Author: Aries Sanchez Sulit
// Date Created: 2019/08/14
// Copyright (c) 2019 Delphyq
#endregion

using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Automation
{
    /// <summary>
    /// A Builder helper class that manages the Build Automation.
    /// This class is also called in environment scripts and jenkins for Build Automation.
    /// </summary>
    public static class Builder
    {
        #region Statics

        /// <summary>
        /// Invokes the Build.
        /// </summary>
        [MenuItem("Automation/Build")]
        public static void Build()
        {
            // TODO: Support control of Build Options. (Platorm, Name, and Targets)
            BuildWindows();
        }

        private static void BuildWindows()
        {
            var scenes = EditorBuildSettings.scenes.Select(s => s.path).ToArray();
            var options = new BuildPlayerOptions
            {
                    scenes           = scenes,
                    locationPathName = "Builds/Windows/Delphyq.exe",
                    target           = BuildTarget.StandaloneWindows,
                    options          = BuildOptions.None
            };

            var result = BuildPipeline.BuildPlayer(options);
            var logs = new StringBuilder();
            logs.Append("Build Result: ");
            logs.Append(result);

            Debug.Log(logs.ToString());
        }

        #endregion Statics
    }
}