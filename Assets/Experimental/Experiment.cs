#region File Header
// Filename: Experiment.cs
// Author: Aries Sanchez Sulit
// Date Created: 2019/08/15
// Copyright (c) 2019 Fortend
#endregion

using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Experimental
{
    #region Data Classes

    public struct Pair
    {
        public bool Value;
        public string Key;
    }

    #endregion Data Classes
    
    public static class Experiment
    {
        #region Static Methods

        private static void Strings()
        {
            foreach (var scene in EditorBuildSettings.scenes)
            {
                Debug.Log($"Status:{scene.enabled} Path:{scene.path}");
            }

            var scenes = EditorBuildSettings.scenes.Select(s => s.path);
            foreach (var scene in scenes)
            {
                Debug.Log($"Scene:{scene}");
            }
        }

        private static void Join()
        {
            var pairs = new List<Pair>()
            {
                    new Pair() { Value = true, Key = "A" },
                    new Pair() { Value = false, Key = "B" },
                    new Pair() { Value = true, Key = "C" },
                    new Pair() { Value = false, Key = "D" },
            };

            var validPairs = string.Join(";", pairs.Where(p => p.Value).Select(p => p.Key));
            Debug.Log($"Valid Pairs:{validPairs}");
        }

        #endregion Static Methods
    }
}